var gl;
var dragging = false;
var lastX, lastY;

window.onload = function init() {
    var canvas = document.getElementById("gl-canvas");
    gl = WebGLUtils.setupWebGL(canvas);
    if(!gl) { alert("WebGL isn't available"); }
    
	
    var dxTotal=0;
	var dyTotal=0;
	var dx=0;
	var dy=0;
	
	canvas.onmousemove = function(event) {
        if(!dragging) return;

        var x = event.clientX;
        var y = event.clientY;

        dx = x - lastX;
        dy = y - lastY;

        dx = dx * 2 / canvas.width;
        dy = dy * 2 / canvas.height;
		
		
		gl.uniform1f(dxLoc, dxTotal+dx);
		gl.uniform1f(dyLoc, dyTotal+dy);
       // alert(dx + " " + dy);
    }

    canvas.onmousedown = function(event) {
        dragging = true;
        lastX = event.clientX;
        lastY = event.clientY;
		dx=0;
		dy=0;
    }

    canvas.onmouseup = function(event) {
        dragging = false;
		dxTotal+=dx;
		dyTotal+=dy;
    }

    // Three vertices
    var vertices = [
        vec2(-1,1),
        vec2(-1,-1),
        vec2(1,1),
        vec2(1,-1)
    ];
    
    // Configure WebGL
    gl.viewport(0,0,canvas.width, canvas.height);
    gl.clearColor(1.0, 1.0, 1.0, 1.0);
    
    // Load shaders and initialize attribute buffers
    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

	var dxLoc=gl.getUniformLocation(program, "dx");
	var dyLoc =gl.getUniformLocation(program, "dy");
	
    // Load the data into the GPU
    var bufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

    // Associate our shader variables with our data buffer
    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);
    render();
}

function render() {
	requestAnimationFrame(render);
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.drawArrays(gl.TRIANGLE_STRIP, 0, 4);
}
