var gl;
var program;

var funcLoc, func=0.0;

window.onload = function init() {
    var canvas = document.getElementById("gl-canvas");
    gl = WebGLUtils.setupWebGL(canvas);
    if(!gl) { alert("WebGL isn't available"); }
    
    // 10.000 vertices
    var vertices = [];

    for(let i=0; i<10000; i++)
        vertices.push(i);
    
    // Configure WebGL
    gl.viewport(0,0,canvas.width, canvas.height);
    gl.clearColor(1.0, 1.0, 1.0, 1.0);
    
    // Load shaders and initialize attribute buffers
    program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    // Load the data into the GPU
    var bufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferId);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(vertices), gl.STATIC_DRAW);

    // Associate our shader variables with our data buffer
    var vTimeSample = gl.getAttribLocation(program, "vTimeSample");
    gl.vertexAttribPointer(vTimeSample, 1, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vTimeSample);

    funcLoc = gl.getUniformLocation(program, "func");

    var mySelect = document.getElementById("func");
    mySelect.onchange = function () {
        func = parseFloat(this.value);
    };
    render();

}

function render() {
    window.requestAnimationFrame(render);

    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.uniform1f(funcLoc, func);
    gl.drawArrays(gl.LINE_STRIP, 0, 10000);
}