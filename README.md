# README #

### What is this repository for? ###

* Solutions to the exercises proposed during the lab sessions
* Version 1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone this repository
* Open the cloned directory in Visual Studio Code/Brackets/etc...
* Right click the corresponding .html for the intended example you want to run
* Select "Open With Live Server" (Visual Studio Code)
* Instructions depend on the Editor you are using

### Author(s) ###

* Fernando Birra
